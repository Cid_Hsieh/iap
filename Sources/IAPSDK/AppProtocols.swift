

import Foundation
import StoreKit

// MARK: - DiscloseView

protocol DiscloseView {
    func show()
    func hide()
}

// MARK: - EnableItem

protocol EnableItem {
    func enable()
    func disable()
}

// MARK: - StoreManagerDelegate

public protocol StoreManagerDelegate: AnyObject {
    /// Provides the delegate with the App Store's response.
    func storeManagerDidReceiveResponse(_ response: [Section])
    
    /// Provides the delegate with the error encountered during the product request.
    func storeManagerDidReceiveMessage(_ message: String)
}

// MARK: - StoreObserverDelegate

public protocol StoreObserverDelegate: AnyObject {
    /// Tells the delegate that the restore operation was successful.
    func storeObserverRestoreDidSucceed(_ restored: [SKPaymentTransaction])
    
    /// Provides the delegate with messages.
    func storeObserverDidReceiveMessage(_ message: String)
    
    /// Handles successful purchase transactions.
    func storeOnserverHandlePurchased()
}

// MARK: - SettingsDelegate

public protocol SettingsDelegate: AnyObject {
    /// Tells the delegate that the user has requested the restoration of their purchases.
    func settingDidSelectRestore()
}
